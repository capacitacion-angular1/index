# Capacitacion Angular ITConcultancy

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## Contenido!

  - Introducción
  - Instalación y configuraciones Mínimas 
  - HTML/CSS/JavaScript-TypeScript
  - Configurar el entorno local y espacio de trabajo
  - Angular CLI
  - Arquitectura Angular
  - Componentes
  - Módulos
  - Interpolación
  - Property Binding
  - Enlace de datos
  - Ciclo de vida de un Componente
  - @Input() y @Output()
  - Rutas en Angular (SPA)
  - Lazy-loading
  - Formularios Reactivos y de Plantilla
  - Validación de formulario

## Ejercicios

| Editor | Link |
| ------ | ------ |
| Componentes | [https://gitlab.com/capacitacion-angular1/components/]|
| Rutas | [https://gitlab.com/capacitacion-angular1/rutas/] |
| Formularios | [https://gitlab.com/capacitacion-angular1/formularios/] |

### Comandos principales

```sh
$ node -v
$ npm -v
$ ng --version
$ npm install -g @angular/cli
$ npm install
$ npm run start
$ npm run build
```

### Editores Recomentados

| Editor | Link |
| ------ | ------ |
| Visual Studio Code | [https://code.visualstudio.com/]|
| WebStorm | [https://www.jetbrains.com/] |


Licencia 
----

MIT


**Free Software, Hell Yeah!**